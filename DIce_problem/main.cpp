// Define Combination sumbol : C(n,x) = n! /x! * (n-x)!   n>x

// Taking N=7 as an example, at least we need two variables( x1+x2 = 7) and we need 7 variables at most (x1+x2+x3+x4+x5+x6+x7 = 7), so our range is between 2 and 7. And then we need to discuss each variable number seperately. Thus the question can be shown as below:
//  1. X1+X2 = 7(2 var)
//  2. X1+X2+X3 = 7(3 var)
//  3. X1+X2+X3+X4 = 7(4 var)
//  4. X1+X2+X3+X4+X5 = 7(5 var)
//  5. X1+X2+X3+X4+X5+X6 = 7(6 var)
//  6. X1+X2+X3+X4+X5+X6+X7 = 7(7 var)

//  For each var: 1=<Xn<=6. We name X1' = X1-1, X2' = X2-1.....X7' = X7-1.
//  X1+X2 = 7 can be converted to X1'+ X2' = 5. Thus the original problem can be written as below:
//  1. X1+X2 = 5
//  2. X1+X2+X3 = 4
//  3. X1+X2+X3+X4 = 3
//  4. X1+X2+X3+X4+X5 = 2
//  5. X1+X2+X3+X4+X5+X6 = 1
//  6. X1+X2+X3+X4+X5+X6+X7 = 0
//  Now each variable:  0<=Xn<=6

//Further, we use combination and Binomial theorem concept.
//To find the possible answer of X1+X2 = 5, we use combination C(6,5), and
//X1+X2+X3 = 4 we use C(6,4) and then C(6,3), C(6,2), C(6,1), C(6,0).
//So we want the sumary of C(6,5)+C(6,4)+C(6,3)+C(6,2)+C(6,1)+C(6,0)
//We found we are also retrieving the binomial coefficient.
// Thus the problem can be written as (1+1)^ 6 -1 (minus 1 means we don't have C(6,6)).
// Finally, we can use this formula to compute the answer(Given n spaces)
// : 2^ (n-1) -1



#include <iostream>
#include <math.h>
using namespace std;
int main(int argc, const char * argv[]) {
//decide at most variable numbers--- n
//decide at least variable numbers--
//form the range (at_least, at_most)
//Making the random number ranged between 1 and 6;
//itrator each variable numbers
    int n;// input n spaces
    int at_most;
    int at_least;
    int range = at_most - at_least+1;
    int VariableNum; 
    int count = 0;
    n=610;
    at_most = n;
    bool judge = true;
    int initial = n;
    while(judge == true){
     if(n*6>initial)
      n--;
    
     else
     {
         judge = false;
         at_least = n+1;
     }
    }
    
    double ans = pow(2, initial-1) -1 ;
    printf("%lf\n",ans);
    return 0;
}
